#!/bin/bash

sudo yum update -y 
sudo amazon-linux-extras install -y docker
sudo systemctl enable docker.service --now
sudo usermod -aG docker ec2-user
