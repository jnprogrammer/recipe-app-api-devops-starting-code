variable "prefix" {
  default     = "raad"
  description = "This is to identify what resoruces are terraform made by the project title"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "joshua@holdcryptoinc.com"
}

variable "db_username" {
  description = "Username for rds postgres instance"
}

variable "db_password" {
  description = "password for rds postgres instance"
}

variable "bastion_key_name" {
  default     = "recipe-app-api-devops-bastion"
  description = "Name of the key pair in aws"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "285327730139.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "285327730139.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "judgehigh.com"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}